# AIMET CI

The AIMET project is hosted at https://github.com/quic/aimet

This repo holds some CI and build automation for AIMET, e.g. a job that generates docker images for the various variants at https://github.com/quic/aimet/tree/develop/Jenkins and then posts them to CodeLinaro's artifact server: https://artifacts.codelinaro.org/ui/native/codelinaro-aimet/. 
